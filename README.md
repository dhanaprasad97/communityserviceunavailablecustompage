# README #

This asset contains an visual force page which is displayed as custom page when community portal is dowm i.e under maintaince


### How do I get set up? ###

* Add the visual force page in this asset to your salesforce org
* Go to sites  from setup and open it by clicking Edit
* In the edit you will find inactive community field
* Click on search icon to right of it and select the visual force page which you have added and save it
* To test deactiavate the site and open the community page you will be displayed with visual force page
